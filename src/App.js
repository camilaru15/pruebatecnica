import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  } from 'react-router-dom'
import './App.css';
import Homepage from './screens/HomePage';
import ResultPage from './screens/ResultPage';

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path='/' Component={Homepage} exact></Route>
          <Route path='/ResultPage' Component={ResultPage}></Route>
        </Routes>
      </Router>
    </>
  );
}

export default App;
