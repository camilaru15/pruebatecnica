import React, { useState } from 'react';
import './Search.css'
import {Link} from 'react-router-dom'
import Data from '../../utils/Data';
import ResultPage from '../../screens/ResultPage';



export default function Search() {
  const[data,setData]=useState(null)
  const [search, setSearch] = useState("");

  const SearchAnimal = () => {
    setData(Data)
    return(
      <ResultPage data={data}/>
    )
  }
  
  return (
    <div>
          <div className='Search'>
            <div>
              <input type="search" className='Search-text' value={search} onChange={(e)=>{setSearch(e.target.value)}}/> 
            </div> 
            <div className='Search-button'>
              <Link to="/ResultPage" onClick={()=>SearchAnimal} className='Search-button_text' data={data} >Buscar</Link>
            </div>
          </div>
    </div>
  )
}




