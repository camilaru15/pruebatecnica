import React, { useEffect, useState } from 'react'
import '../styles/Styles.css';
import logo from '../assets/logo.png';
import icono from '../assets/icono.png';
import menu from '../assets/menu.PNG';
import Data from '../utils/Data'
import { Link } from 'react-router-dom';

export default function ResultPage() {
  console.log("data result page")
  const[search,setSearch]= useState("")
  const[data,setData]=useState([])
  const[preview,setPreview]=useState(<></>)

  useEffect(()=>{
    setData(Data)
  },[])

  const previewAnimal = (row) =>{
    console.log("row",row)
    let list=[]
      list.push(
        <div className='Preview'>
          <img src={row.image} width={'100%'} alt='img'/>
          <p className='preview-content_url'>{row.url}</p>
          <h1 className='preview-content_title'>{row.title}</h1>
          <p className='preview-content_text'>{row.description}</p>
        </div>
      )
    setPreview(list)
  }

  return (
    <div className='content'>
      <div className="Header-search">
        <Link to='/'>
        <img src={logo} className="Header-logo" alt="logo" />
        </Link>        
        <div  className='Header-search-tab'>
          <input type="search" className='Header-search-tab_content' value={search} onChange={(e)=>{setSearch(e.target.value)}}/> 
        </div>
        <div>
          <img src={menu} className="Header-icono" alt="icono"/>
          <img src={icono} className="Header-icono" alt="icono"/>
        </div>  
      </div>
      <div className='container'>
        <div className='list'>
          {
            data.filter((row) =>{
              if(search === " "){
              console.log("sin datos")
              }
              else{
                return row
              }
            })
            .map((row,i)=>{
            return(
              <div key={i} className='list-content'>
              <p className='list-content_url'>{row.url}</p>
              <Link onClick={()=>{previewAnimal(row)}} className='list-content_title' >{row.title}</Link>
              <p className='list-content_text'>{row.description}</p>
              <br></br>
              </div>
              )
            })
          }
        </div>
        <div className='Preview-card'>
          {preview}
        </div>
      </div>   
    </div>
    
  )
}
