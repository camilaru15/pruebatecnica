import React from 'react'
import logo from '../assets/logo.png';
import icono from '../assets/icono.png';
import menu from '../assets/menu.PNG';
import Search from '../components/search/Search';
import '../styles/Styles.css';

export default function Homepage() {
    return (
        <div>
          <div className="Header">
            <p><b>Agile content </b>Frontend test</p>
            <div>
              <img src={menu} className="Header-icono" alt="icono"/>
              <img src={icono} className="Header-icono" alt="icono"/>
            </div>          
          </div>
          <div className="Body">
            <img src={logo} className="Body-logo" alt="logo" />
            <Search/>
          </div>
          <div className="Footer">
            <h3>@Google 2021</h3>
            <h3>version 0.1.0</h3>
          </div>
        </div>
      );
}
